import Image1 from "../../assets/project1.png";
import Image2 from "../../assets/project2.png";
import Image3 from "../../assets/project3.png";
import Image4 from "../../assets/project4.png";
import Image5 from "../../assets/project5.png";
import Image6 from "../../assets/project6.png";

const data = [
  {
    id: 1,
    category: "frontend",
    image: Image1,
    title: "Personal Portfolio Vol.1 (Frontend)",
    desc: "Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam nihil, eveniet aliquid culpa officia aut!",
    demo: "https://portfolio-vol1.netlify.app/",
    github: "https://github.com/yusuffikri/portfolio-boostrap5",
  },
  {
    id: 2,
    category: "frontend",
    image: Image2,
    title: "Starbucks Landing Page (Frontend)",
    desc: "Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam nihil, eveniet aliquid culpa officia aut!",
    demo: "https://starbucks-demosite.netlify.app/",
    github: "https://github.com/yusuffikri/starbucks-landingpage",
  },
  {
    id: 3,
    category: "frontend",
    image: Image3,
    title: "Personal Portfolio Vol.2 (Frontend)",
    desc: "Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam nihil, eveniet aliquid culpa officia aut!",
    demo: "https://yusuffikri.github.io/",
    github: "https://github.com/yusuffikri/portfolio-react-js",
  },
  {
    id: 4,
    category: "frontend",
    image: Image4,
    title: "Calculator Apps (Frontend)",
    desc: "Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam nihil, eveniet aliquid culpa officia aut!",
    demo: "https://calculator-demos.netlify.app/",
    github: "https://github.com/yusuffikri/calculator",
  },
  {
    id: 5,
    category: "Mini Games",
    image: Image5,
    title: "Tic Tac Toes (Mini Games)",
    desc: "Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam nihil, eveniet aliquid culpa officia aut! ",
    demo: "https://tictactoes-demo.netlify.app/",
    github: "https://github.com/yusuffikri/tictactoe-js",
  },
  {
    id: 6,
    category: "frontend",
    image: Image6,
    title: "Personal Portfolio Vol.3 (Frontend)",
    desc: "Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam nihil, eveniet aliquid culpa officia aut!",
    demo: "https://yusuffikri.github.io/",
    github: "https://github.com/yusuffikri",
  },
];

export default data;
